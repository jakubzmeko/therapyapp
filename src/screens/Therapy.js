import React, { Component } from 'react';
import { View, ScrollView, TouchableOpacity, Alert, Text } from 'react-native';
import { Appbar, List, Button, Divider, Title } from 'react-native-paper';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faStar } from '@fortawesome/free-solid-svg-icons';
import { SwipeListView } from 'react-native-swipe-list-view';
import { openDatabase } from 'react-native-sqlite-storage';
import moment from 'moment';
import AsyncStorage from '@react-native-community/async-storage';
//Connction to access the pre-populated therapy_db.db
var db = openDatabase({ name: 'therapy_db1.db', createFromLocation: 1 });

import styles from '../styles/styles';

export default class Therapy extends Component {
  constructor(props) {
    super(props);

    this.state = {
      tasks: [],
      child: props.navigation.getParam('child'),
    };
  }

  componentDidMount() {
    this.loadTasksTherapy();
    this.getTherapist();
  }

  getTherapist = async () => {
    try {
      const therapistName = await AsyncStorage.getItem('therapist');
      if (therapistName !== null) {
        this.setState({ therapistName });
      }
    } catch (e) {
      // error reading value
    }
  };

  loadTasksTherapy() {
    let that = this;
    db.transaction(tx => {
      tx.executeSql(
        'SELECT therapies.id, therapies.id_child, therapies.id_task, therapies.done_1, therapies.done_2, therapies.done_3, therapies.is_active, tasks.task_name, tasks.category FROM therapies INNER JOIN tasks on tasks.id = therapies.id_task  where therapies.id_child=? and therapies.is_active="true"',
        [that.state.child.id],
        (tx, results) => {
          var tasks = [];
          for (let i = 0; i < results.rows.length; ++i) {
            tasks.push(results.rows.item(i));
          }
          this.setState({ tasks });
        },
      );
    });
  }

  addTaskToTherapy(task) {
    let that = this;
    let now = moment(new Date()).format('D.M.YYYY, H:mm');

    db.transaction(function(tx) {
      tx.executeSql(
        'INSERT INTO therapies (id_child, id_task, added, therapist, is_active) VALUES (?,?,?,?,?)',
        [that.state.child.id, task.id, now, that.state.therapistName, 'true'],
        (tx, results) => {
          that.loadTasksTherapy();

          if (results.rowsAffected === 0) {
            alert('Pridanie zlyhalo, skúste znova');
          }
        },
      );
    });
  }

  updateTaskProgress(task) {
    let that = this;
    let now = moment(new Date()).format('D.M.YYYY, H:mm');
    let done_1 = task.done_1 ? task.done_1 : now;
    let done_2 = task.done_1 ? now : '';
    let done_3 = task.done_2 ? now : '';
    let is_active = task.done_2 ? 'false' : 'true';

    db.transaction(tx => {
      tx.executeSql(
        'UPDATE therapies set done_1=?, done_2=?, done_3=?, is_active=? where id=?',
        [done_1, done_2, done_3, is_active, task.id],
        (tx, results) => {
          if (results.rowsAffected > 0) {
            that.loadTasksTherapy();
          } else {
            alert('Updation Failed');
          }
        },
      );
    });
  }

  removeTaskFromTherapy(task) {
    let that = this;
    let now = moment(new Date()).format('D.M.YYYY, H:mm');

    db.transaction(tx => {
      tx.executeSql(
        'UPDATE therapies set removed=?, is_active=? where id=?',
        [now, 'false', task.id],
        (tx, results) => {
          if (results.rowsAffected > 0) {
            that.loadTasksTherapy();
          } else {
            alert('Updation Failed');
          }
        },
      );
    });
  }

  renderCategoryView(name, tasks) {
    if (this.state.tasks.length) {
      if (tasks.length) {
        return (
          <View>
            <List.Subheader>
              <Title>{name}</Title>
            </List.Subheader>
            <SwipeListView
              data={tasks}
              renderItem={(data, rowMap) => (
                <View style={styles.rowFront}>
                  <List.Item
                    title={data.item.task_name}
                    right={() => (
                      <TouchableOpacity
                        style={{ flexDirection: 'row', alignItems: 'center' }}
                        onPress={() => this.updateTaskProgress(data.item)}
                      >
                        <FontAwesomeIcon
                          style={{ marginLeft: 5 }}
                          icon={faStar}
                          color={data.item.done_1 ? '#2982ed' : 'gray'}
                          size={20}
                        />
                        <FontAwesomeIcon
                          style={{ marginLeft: 5 }}
                          icon={faStar}
                          color={data.item.done_2 ? '#2982ed' : 'gray'}
                          size={20}
                        />
                        <FontAwesomeIcon
                          style={{ marginLeft: 5 }}
                          icon={faStar}
                          size={20}
                          color={data.item.done_3 ? '#2982ed' : 'gray'}
                        />
                      </TouchableOpacity>
                    )}
                  />
                  <Divider />
                </View>
              )}
              renderHiddenItem={(data, rowMap) => (
                <View style={styles.rowBack}>
                  <TouchableOpacity
                    style={[styles.backRightBtn, styles.backRightBtnRight]}
                    onPress={() => {
                      Alert.alert('Vymazať dieťa', 'Ste si sitý, že chcete odstrániť dieťa?', [
                        {
                          text: 'Zrušiť',
                          onPress: () => {
                            console.log('canceled');
                          },
                          style: 'cancel',
                        },
                        {
                          text: 'Áno, chcem',
                          onPress: () => {
                            this.removeTaskFromTherapy(data.item);
                          },
                        },
                      ]);
                    }}
                  >
                    <Text style={styles.backTextWhite}>Vymazať</Text>
                  </TouchableOpacity>
                </View>
              )}
              rightOpenValue={-75}
              keyExtractor={item => item.id.toString()}
            />
          </View>
        );
      }
    }
  }

  render() {
    return (
      <View style={{ flex: 1, backgroundColor: '#ffffff' }}>
        <Appbar.Header>
          <Appbar.BackAction onPress={() => this.props.navigation.goBack()} />
          <Appbar.Content title="Terapia" subtitle="2.3.2020 13:00" />
        </Appbar.Header>
        <View style={styles.buttonWrap}>
          <Button
            icon="plus"
            mode="contained"
            onPress={() =>
              this.props.navigation.navigate('TasksListAdd', {
                addTaskToTherapy: this.addTaskToTherapy.bind(this),
              })
            }
          >
            Pridať úlohu do terapie
          </Button>
        </View>
        <ScrollView style={{ height: '100%' }}>
          {this.renderCategoryView(
            'Takty',
            this.state.tasks.filter(task => task.category === 'Takty'),
          )}
          {this.renderCategoryView(
            'Mandy',
            this.state.tasks.filter(task => task.category === 'Mandy'),
          )}
          {this.renderCategoryView(
            'Inštrukcie',
            this.state.tasks.filter(task => task.category === 'Inštrukcie'),
          )}
          {this.renderCategoryView(
            'Intraverbál',
            this.state.tasks.filter(task => task.category === 'Intraverbál'),
          )}
          {this.renderCategoryView(
            'Imitácie',
            this.state.tasks.filter(task => task.category === 'Imitácie'),
          )}
        </ScrollView>
      </View>
    );
  }
}
