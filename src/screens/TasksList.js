import React, { Component } from 'react';
import { View, StyleSheet, ScrollView, Alert, FlatList } from 'react-native';
import {
  Appbar,
  List,
  Title,
  Divider,
  Portal,
  Dialog,
  Button,
  TextInput,
  HelperText,
} from 'react-native-paper';
import RNPickerSelect from 'react-native-picker-select';
import AsyncStorage from '@react-native-community/async-storage';
import { openDatabase } from 'react-native-sqlite-storage';
//Connction to access the pre-populated therapy_db.db
var db = openDatabase({ name: 'therapy_db1.db', createFromLocation: 1 });

import styles from '../styles/styles';

export default class TasksList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      tasks: [],
      visibleDialog: false,
      taskName: '',
      taskTheme: '',
      category: '',
      taskDifficulty: '',
      therapistName: '',
    };
  }

  componentDidMount() {
    this.loadTasks();
    this.getTherapist();
  }

  getTherapist = async () => {
    try {
      const therapistName = await AsyncStorage.getItem('therapist');
      if (therapistName !== null) {
        this.setState({ therapistName });
      }
    } catch (e) {
      // error reading value
    }
  };

  _showDialog = () => this.setState({ visibleDialog: true });

  _hideDialog = () => this.setState({ visibleDialog: false });

  loadTasks() {
    db.transaction(tx => {
      tx.executeSql('SELECT * FROM tasks', [], (tx, results) => {
        var tasks = [];
        for (let i = 0; i < results.rows.length; ++i) {
          tasks.push(results.rows.item(i));
        }
        this.setState({ tasks });
      });
    });
  }

  createTask = () => {
    let that = this;

    if (
      this.state.taskName.length &&
      this.state.category.length &&
      this.state.taskDifficulty.length
    ) {
      db.transaction(function(tx) {
        tx.executeSql(
          'INSERT INTO tasks (task_name, category, difficulty, theme, author) VALUES (?,?,?,?,?)',
          [
            that.state.taskName,
            that.state.category,
            that.state.taskDifficulty,
            that.state.taskTheme,
            that.state.therapistName,
          ],
          (tx, results) => {
            alert('Úloha pridaná');
            that._hideDialog();
            that.loadTasks();

            if (results.rowsAffected === 0) {
              alert('Pridanie úlohy zlyhalo, skúste znova');
            }
          },
        );
      });
    } else {
      alert('Prosím vyplňte povinné údaje povinné údaje');
      return false;
    }
  };

  renderCategoryView(name, tasks) {
    if (this.state.tasks.length) {
      if (tasks.length) {
        return (
          <View>
            <List.Subheader>
              <Title>{name}</Title>
            </List.Subheader>
            <FlatList
              data={tasks}
              renderItem={({ item }) => (
                <View>
                  <List.Item title={item.task_name} />
                  <Divider />
                </View>
              )}
              keyExtractor={item => item.id}
            />
          </View>
        );
      }
    }
  }

  render() {
    return (
      <View style={{ flex: 1, backgroundColor: '#ffffff' }}>
        <Appbar.Header>
          <Appbar.Content title="Zoznam všetkých úloh" />
        </Appbar.Header>
        <View style={styles.buttonWrap}>
          <Button icon="plus" mode="contained" onPress={() => this._showDialog()}>
            Pridať úlohu
          </Button>
        </View>
        <ScrollView style={{ height: '100%' }}>
          {this.renderCategoryView(
            'Takty',
            this.state.tasks.filter(task => task.category === 'Takty'),
          )}
          {this.renderCategoryView(
            'Mandy',
            this.state.tasks.filter(task => task.category === 'Mandy'),
          )}
          {this.renderCategoryView(
            'Inštrukcie',
            this.state.tasks.filter(task => task.category === 'Inštrukcie'),
          )}
          {this.renderCategoryView(
            'Intraverbál',
            this.state.tasks.filter(task => task.category === 'Intraverbál'),
          )}
          {this.renderCategoryView(
            'Imitácie',
            this.state.tasks.filter(task => task.category === 'Imitácie'),
          )}
        </ScrollView>
        <Portal>
          <Dialog visible={this.state.visibleDialog} onDismiss={this._hideDialog}>
            <Dialog.Title>Pridanie novej úlohy</Dialog.Title>
            <Dialog.Content>
              <TextInput
                style={{ height: 45 }}
                label="Názov úlohy"
                mode="outlined"
                value={this.state.taskName}
                onChangeText={taskName => this.setState({ taskName })}
              />
              <HelperText style={{ marginBottom: 10 }} type="info">
                povinné pole
              </HelperText>
              <TextInput
                style={{ height: 45, marginBottom: 10 }}
                label="Téma"
                mode="outlined"
                value={this.state.lastName}
                onChangeText={taskTheme => this.setState({ taskTheme })}
              />
              <RNPickerSelect
                onValueChange={category => this.setState({ category })}
                placeholder={{ label: 'Vyberte kategóriu', value: null }}
                items={[
                  { label: 'Takty', value: 'Takty' },
                  { label: 'Mandy', value: 'Mandy' },
                  { label: 'Inštrukcie', value: 'Inštrukcie' },
                  { label: 'Intraverbál', value: 'Intraverbál' },
                  { label: 'Imitácie', value: 'Imitácie' },
                ]}
                style={{
                  ...pickerSelectStyles,
                  placeholder: {
                    color: '#7f7f7f',
                    fontSize: 16,
                  },
                }}
              />
              <HelperText style={{ marginBottom: 10 }} type="info">
                povinné pole
              </HelperText>
              <RNPickerSelect
                onValueChange={taskDifficulty => this.setState({ taskDifficulty })}
                placeholder={{ label: 'Vyberte obťiažnosť', value: null }}
                items={[
                  { label: 'Základné', value: 'Základné' },
                  { label: 'Pokročilé', value: 'Pokročilé' },
                ]}
                style={{
                  ...pickerSelectStyles,
                  placeholder: {
                    color: '#7f7f7f',
                    fontSize: 16,
                  },
                }}
              />
              <HelperText style={{ marginBottom: 10 }} type="info">
                povinné pole
              </HelperText>
            </Dialog.Content>
            <Dialog.Actions>
              <Button onPress={() => this.createTask()}>Pridať</Button>
            </Dialog.Actions>
          </Dialog>
        </Portal>
      </View>
    );
  }
}

const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 16,
    paddingVertical: 12,
    paddingHorizontal: 10,
    borderWidth: 1,
    borderColor: 'gray',
    borderRadius: 4,
    color: 'black',
    paddingRight: 30,
    backgroundColor: '#f6f6f6',
  },
  inputAndroid: {
    fontSize: 16,
    paddingHorizontal: 10,
    paddingVertical: 8,
    borderWidth: 0.5,
    borderColor: 'gray',
    borderRadius: 8,
    color: 'black',
    paddingRight: 30,
    backgroundColor: '#f6f6f6',
  },
});
