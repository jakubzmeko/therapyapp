import React, { Component } from 'react';
import { View, ScrollView, TouchableOpacity, Text, Alert } from 'react-native';
import {
  Appbar,
  List,
  Title,
  Divider,
  Button,
  Portal,
  Dialog,
  TextInput,
} from 'react-native-paper';
import { SwipeListView } from 'react-native-swipe-list-view';
import { openDatabase } from 'react-native-sqlite-storage';
//Connction to access the pre-populated therapy_db1.db
var db = openDatabase({ name: 'therapy_db1.db', createFromLocation: 1 });

import styles from '../styles/styles';

export default class Children extends Component {
  constructor(props) {
    super(props);

    this.state = {
      visibleMenu: false,
      visibleDialog: false,
      children: [],
      name: '',
      surname: '',
      birth_day: '',
      chidlInfo: '',
    };
  }

  _showDialog = () => this.setState({ visibleDialog: true });

  _hideDialog = () => this.setState({ visibleDialog: false });

  _openMenu = () => this.setState({ visibleMenu: true });

  _closeMenu = () => this.setState({ visibleMenu: false });

  componentDidMount() {
    this.loadChildren();
  }

  loadChildren() {
    db.transaction(tx => {
      tx.executeSql('SELECT * FROM children', [], (tx, results) => {
        var children = [];
        for (let i = 0; i < results.rows.length; ++i) {
          children.push(results.rows.item(i));
        }
        this.setState({ children: children });
      });
    });
  }

  createChild = () => {
    let that = this;

    if (this.state.name.length && this.state.surname.length) {
      db.transaction(function(tx) {
        tx.executeSql(
          'INSERT INTO children (name, surname, birth_day, child_info) VALUES (?,?,?,?)',
          [that.state.name, that.state.surname, that.state.birth_day, that.state.chidlInfo],
          (tx, results) => {
            that._hideDialog();
            that.loadChildren();

            if (results.rowsAffected === 0) {
              alert('Pridanie dieťaťa zlyhalo, skúste znova');
            }
          },
        );
      });
    } else {
      alert('Meno, priezvisko a dátum narodenia su povinné údaje');
      return false;
    }
  };

  deleteChild(child_id) {
    let that = this;
    db.transaction(tx => {
      tx.executeSql('DELETE FROM children where id=?', [child_id], (tx, results) => {
        if (results.rowsAffected > 0) {
          this.loadChildren();
        } else {
          alert('Niekde nastala chyba, skúste znova');
        }
      });
    });
  }

  renderChildrenView() {
    if (this.state.children) {
      return (
        <ScrollView style={{ height: '100%' }}>
          <SwipeListView
            data={this.state.children}
            renderItem={(data, rowMap) => (
              <View style={styles.rowFront}>
                <List.Item
                  title={data.item.name + ' ' + data.item.surname}
                  right={props => <List.Icon {...props} icon="chevron-right" />}
                  onPress={() =>
                    this.props.navigation.navigate('Therapies', {
                      child: data.item,
                      loadChildren: this.loadChildren.bind(this),
                    })
                  }
                />
                <Divider />
              </View>
            )}
            renderHiddenItem={(data, rowMap) => (
              <View style={styles.rowBack}>
                <TouchableOpacity
                  style={[styles.backRightBtn, styles.backRightBtnRight]}
                  onPress={() => {
                    Alert.alert('Vymazať dieťa', 'Ste si sitý, že chcete odstrániť dieťa?', [
                      {
                        text: 'Zrušiť',
                        onPress: () => {
                          this._closeMenu();
                        },
                        style: 'cancel',
                      },
                      {
                        text: 'Áno, chcem',
                        onPress: () => {
                          this.deleteChild(data.item.id);
                        },
                      },
                    ]);
                  }}
                >
                  <Text style={styles.backTextWhite}>Vymazať</Text>
                </TouchableOpacity>
              </View>
            )}
            rightOpenValue={-75}
            keyExtractor={item => item.id.toString()}
          />
        </ScrollView>
      );
    }
  }

  render() {
    return (
      <View style={{ flex: 1, backgroundColor: '#ffffff' }}>
        <Appbar.Header>
          <Appbar.Content title="Deti" />
        </Appbar.Header>
        <View style={styles.buttonWrap}>
          <Button icon="plus" mode="contained" onPress={() => this._showDialog()}>
            Pridať dieťa
          </Button>
        </View>
        <List.Subheader>
          <Title>Výber dieťaťa</Title>
        </List.Subheader>
        <Divider />
        {this.renderChildrenView()}

        <Portal>
          <Dialog visible={this.state.visibleDialog} onDismiss={this._hideDialog}>
            <Dialog.Title>Pridať dieťa</Dialog.Title>
            <Dialog.Content>
              <TextInput
                style={{ height: 45, marginBottom: 10 }}
                label="Krstné meno"
                mode="outlined"
                value={this.state.name}
                onChangeText={name => this.setState({ name })}
              />
              <TextInput
                style={{ height: 45, marginBottom: 10 }}
                label="Priezvisko"
                mode="outlined"
                value={this.state.surname}
                onChangeText={surname => this.setState({ surname })}
              />
              <TextInput
                style={{ height: 45, marginBottom: 10 }}
                label="Dátum narodenia"
                mode="outlined"
                value={this.state.birth_day}
                onChangeText={birth_day => this.setState({ birth_day })}
              />
              <TextInput
                label="Ďalšie info"
                mode="outlined"
                value={this.state.chidlInfo}
                multiline={true}
                onChangeText={chidlInfo => this.setState({ chidlInfo })}
              />
            </Dialog.Content>
            <Dialog.Actions>
              <Button
                onPress={() => {
                  this.createChild();
                }}
              >
                Uložiť
              </Button>
            </Dialog.Actions>
          </Dialog>
        </Portal>
      </View>
    );
  }
}
