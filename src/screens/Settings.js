import React, { Component } from 'react';
import { View, ScrollView } from 'react-native';
import { Appbar, TextInput } from 'react-native-paper';
import AsyncStorage from '@react-native-community/async-storage';
import styles from '../styles/styles';

export default class Settings extends Component {
  constructor(props) {
    super(props);

    this.state = {
      therapistName: '',
    };
  }

  componentDidMount() {
    this.getData();
  }

  storeData = async () => {
    try {
      await AsyncStorage.setItem('therapist', this.state.therapistName);
    } catch (e) {
      // saving error
    }
  };

  getData = async () => {
    try {
      const therapist = await AsyncStorage.getItem('therapist');
      if (therapist !== null) {
        this.setState({ therapistName: therapist });
      }
    } catch (e) {
      // error reading value
    }
  };

  render() {
    return (
      <View style={{ flex: 1, backgroundColor: '#ffffff' }}>
        <Appbar.Header>
          <Appbar.Content title="Nastavenia" />
        </Appbar.Header>
        <ScrollView>
          <View style={styles.buttonWrap}>
            <TextInput
              style={{ height: 45, marginBottom: 10 }}
              label="Meno terapeuta"
              mode="outlined"
              value={this.state.therapistName}
              onChangeText={therapistName => this.setState({ therapistName })}
              onBlur={() => this.storeData()}
            />
          </View>
        </ScrollView>
      </View>
    );
  }
}
