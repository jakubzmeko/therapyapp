import React, { Component } from 'react';
import { View, SafeAreaView, ScrollView, Alert, TouchableOpacity, Text } from 'react-native';
import { Appbar, List, Title, Divider, Menu, Dialog, Button, TextInput } from 'react-native-paper';
import { SwipeListView } from 'react-native-swipe-list-view';
import { openDatabase } from 'react-native-sqlite-storage';
import moment from 'moment';
//Connction to access the pre-populated therapy_db.db
var db = openDatabase({ name: 'therapy_db1.db', createFromLocation: 1 });

import styles from '../styles/styles';

export default class Therapies extends Component {
  constructor(props) {
    super(props);

    this.state = {
      visibleMenu: false,
      visibleDialog: false,
      name: props.navigation.getParam('child').name,
      surname: props.navigation.getParam('child').surname,
      birth_day: props.navigation.getParam('child').birth_day,
      chidlInfo: props.navigation.getParam('child').child_info,
      child: props.navigation.getParam('child'),
      childMain: props.navigation.getParam('child'),
      therapies: [],
    };
  }

  componentDidMount() {
    this.loadTherapies();
  }

  loadTherapies() {
    let that = this;
    db.transaction(tx => {
      tx.executeSql(
        'SELECT * FROM therapy_date WHERE id_child=?',
        [that.state.child.id],
        (tx, results) => {
          var therapies = [];
          for (let i = 0; i < results.rows.length; ++i) {
            therapies.push(results.rows.item(i));
          }
          this.setState({ therapies });
        },
      );
    });
  }

  createTherapy = () => {
    let that = this;

    db.transaction(function(tx) {
      tx.executeSql(
        'INSERT INTO therapy_date (therapy_date, id_child) VALUES (?,?)',
        [moment(new Date()).format('D.M.YYYY, H:mm'), that.state.child.id],
        (tx, results) => {
          that._hideDialog();
          that.loadTherapies();

          if (results.rowsAffected === 0) {
            alert('Pridanie terapie zlyhalo, skúste znova');
          }
        },
      );
    });
  };

  deleteTherapy(therapy_id) {
    let that = this;
    db.transaction(tx => {
      tx.executeSql('DELETE FROM therapy_date where id=?', [therapy_id], (tx, results) => {
        if (results.rowsAffected > 0) {
          that.loadTherapies();
        } else {
          alert('Niekde nastala chyba, skúste znova');
        }
      });
    });
  }

  _showDialog = () => this.setState({ visibleDialog: true });

  _hideDialog = () => this.setState({ visibleDialog: false });

  _openMenu = () => this.setState({ visibleMenu: true });

  _closeMenu = () => this.setState({ visibleMenu: false });

  deleteChild() {
    let that = this;
    db.transaction(tx => {
      tx.executeSql('DELETE FROM children where id=?', [that.state.child.id], (tx, results) => {
        if (results.rowsAffected > 0) {
          that._closeMenu();
          that.props.navigation.state.params.loadChildren();
          that.props.navigation.navigate('Children');
        } else {
          alert('Niekde nastala chyba, skúste znova');
        }
      });
    });
  }

  updateChild() {
    let that = this;
    db.transaction(tx => {
      tx.executeSql(
        'UPDATE children set name=?, surname=?, birth_day=? where id=?',
        [that.state.name, that.state.surname, that.state.birth_day, that.state.child.id],
        (tx, results) => {
          if (results.rowsAffected > 0) {
            that.loadChild();
            that._hideDialog();
          } else {
            alert('Update Failed');
          }
        },
      );
    });
  }

  loadChild() {
    let that = this;
    db.transaction(tx => {
      tx.executeSql('SELECT * FROM children where id=?', [that.state.child.id], (tx, results) => {
        var child = [];
        for (let i = 0; i < results.rows.length; ++i) {
          child.push(results.rows.item(i));
        }
        that.setState({ child: child[0], childMain: child[0] });
      });
    });
  }

  renderTherapiesView() {
    if (this.state.therapies) {
      return (
        <ScrollView style={{ height: '100%' }}>
          <SwipeListView
            data={this.state.therapies}
            renderItem={(data, rowMap) => (
              <View key={data.item.id} style={styles.rowFront}>
                <List.Item
                  title="Terapia"
                  description={data.item.therapy_date}
                  right={props => <List.Icon {...props} icon="chevron-right" />}
                  onPress={() =>
                    this.props.navigation.navigate('Therapy', { child: this.state.child })
                  }
                />
                <Divider />
              </View>
            )}
            renderHiddenItem={(data, rowMap) => (
              <View style={styles.rowBack}>
                <TouchableOpacity
                  style={[styles.backRightBtn, styles.backRightBtnRight]}
                  onPress={() => {
                    Alert.alert('Vymazať terapiu?', 'Ste si istý, že chcete odstrániť terapiu?', [
                      {
                        text: 'Zrušiť',
                        onPress: () => {
                          this._closeMenu();
                        },
                        style: 'cancel',
                      },
                      {
                        text: 'Áno, chcem',
                        onPress: () => {
                          this.deleteTherapy(data.item.id);
                        },
                      },
                    ]);
                  }}
                >
                  <Text style={styles.backTextWhite}>Vymazať</Text>
                </TouchableOpacity>
              </View>
            )}
            rightOpenValue={-75}
            keyExtractor={item => item.id.toString()}
          />
        </ScrollView>
      );
    }
  }

  render() {
    return (
      <View style={{ flex: 1, backgroundColor: '#ffffff' }}>
        <Appbar.Header>
          <Appbar.BackAction onPress={() => this.props.navigation.goBack()} />
          <Appbar.Content title={this.state.childMain.name + ' ' + this.state.childMain.surname} />
          <Menu
            visible={this.state.visibleMenu}
            onDismiss={this._closeMenu}
            anchor={<Appbar.Action icon="dots-vertical" color="#ffffff" onPress={this._openMenu} />}
          >
            <Menu.Item
              style={{ paddingVertical: 0 }}
              icon="plus"
              onPress={() => {
                this.createTherapy();
                this._closeMenu();
              }}
              title="Pridať terapiu pre dieťa"
            />
            <Menu.Item
              style={{ paddingVertical: 0 }}
              icon="pencil"
              onPress={() => {
                this._closeMenu();
                this._showDialog();
              }}
              title="Upraviť dieťa"
            />
            <Menu.Item
              style={{ paddingVertical: 0 }}
              icon="delete"
              onPress={() => {
                Alert.alert('Vymazať dieťa', 'Ste si sitý, že chcete odstrániť dieťa?', [
                  {
                    text: 'Zrušiť',
                    onPress: () => {
                      this._closeMenu();
                    },
                    style: 'cancel',
                  },
                  {
                    text: 'Áno, chcem',
                    onPress: () => {
                      this.deleteChild();
                    },
                  },
                ]);
              }}
              title="Vymazať dieťa"
            />
          </Menu>
        </Appbar.Header>
        <SafeAreaView>
          <List.Subheader>
            <Title>Terapie</Title>
          </List.Subheader>
          <Divider />
          {this.renderTherapiesView()}
        </SafeAreaView>
        <Dialog visible={this.state.visibleDialog} onDismiss={this._hideDialog}>
          <Dialog.Title>Upraviť údaje o dieťati</Dialog.Title>
          <Dialog.Content>
            <TextInput
              style={{ height: 45, marginBottom: 10 }}
              label="Krstné meno"
              mode="outlined"
              value={this.state.name}
              onChangeText={name => this.setState({ name })}
            />
            <TextInput
              style={{ height: 45, marginBottom: 10 }}
              label="Priezvisko"
              mode="outlined"
              value={this.state.surname}
              onChangeText={surname => this.setState({ surname })}
            />
            <TextInput
              style={{ height: 45, marginBottom: 10 }}
              label="Dátum narodenia"
              mode="outlined"
              value={this.state.birth_day}
              onChangeText={birth_day => this.setState({ birth_day })}
            />
            <TextInput
              label="Ďalšie info"
              mode="outlined"
              value={this.state.chidlInfo}
              multiline={true}
              onChangeText={chidlInfo => this.setState({ chidlInfo })}
            />
          </Dialog.Content>
          <Dialog.Actions>
            <Button onPress={() => this.updateChild()}>Uložiť</Button>
          </Dialog.Actions>
        </Dialog>
      </View>
    );
  }
}
