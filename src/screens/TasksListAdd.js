import React, { Component } from 'react';
import { View, ScrollView, FlatList } from 'react-native';
import { Appbar, List, Title, Divider } from 'react-native-paper';
import { openDatabase } from 'react-native-sqlite-storage';
//Connction to access the pre-populated therapy_db.db
var db = openDatabase({ name: 'therapy_db1.db', createFromLocation: 1 });

import styles from '../styles/styles';

export default class TasksListAdd extends Component {
  constructor(props) {
    super(props);

    this.state = {
      tasks: [],
    };
  }

  componentDidMount() {
    this.loadTasks();
  }

  loadTasks() {
    db.transaction(tx => {
      tx.executeSql('SELECT * FROM tasks', [], (tx, results) => {
        var tasks = [];
        for (let i = 0; i < results.rows.length; ++i) {
          tasks.push(results.rows.item(i));
        }
        this.setState({ tasks });
      });
    });
  }

  renderCategoryView(name, tasks) {
    let that = this;
    if (this.state.tasks.length) {
      if (tasks.length) {
        return (
          <View>
            <List.Subheader>
              <Title>{name}</Title>
            </List.Subheader>
            <FlatList
              data={tasks}
              renderItem={({ item }) => (
                <View>
                  <List.Item
                    title={item.task_name}
                    right={props => <List.Icon {...props} icon="plus" />}
                    onPress={() => {
                      that.props.navigation.state.params.addTaskToTherapy(item);
                      that.props.navigation.goBack();
                    }}
                  />
                  <Divider />
                </View>
              )}
              keyExtractor={item => item.id}
            />
          </View>
        );
      }
    }
  }

  render() {
    return (
      <View style={{ flex: 1, backgroundColor: '#ffffff' }}>
        <Appbar.Header>
          <Appbar.BackAction onPress={() => this.props.navigation.goBack()} />
          <Appbar.Content title="Zoznam všetkých úloh" />
        </Appbar.Header>
        <ScrollView style={{ height: '100%' }}>
          {this.renderCategoryView(
            'Takty',
            this.state.tasks.filter(task => task.category === 'Takty'),
          )}
          {this.renderCategoryView(
            'Mandy',
            this.state.tasks.filter(task => task.category === 'Mandy'),
          )}
          {this.renderCategoryView(
            'Inštrukcie',
            this.state.tasks.filter(task => task.category === 'Inštrukcie'),
          )}
          {this.renderCategoryView(
            'Intraverbál',
            this.state.tasks.filter(task => task.category === 'Intraverbál'),
          )}
          {this.renderCategoryView(
            'Imitácie',
            this.state.tasks.filter(task => task.category === 'Imitácie'),
          )}
        </ScrollView>
      </View>
    );
  }
}
