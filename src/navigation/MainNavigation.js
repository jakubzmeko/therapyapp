import React from 'react';
import { createStackNavigator } from 'react-navigation-stack';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faList, faCog, faChild } from '@fortawesome/free-solid-svg-icons';
import Children from '../screens/Children';
import Therapies from '../screens/Therapies';
import Therapy from '../screens/Therapy';
import Settings from '../screens/Settings';
import TasksList from '../screens/TasksList';
import TasksListAdd from '../screens/TasksListAdd';

const ChildrenStack = createStackNavigator(
  {
    Children: { screen: Children },
    Therapies: { screen: Therapies },
    Therapy: { screen: Therapy },
    TasksListAdd: { screen: TasksListAdd },
  },
  {
    headerMode: 'float',
    defaultNavigationOptions: ({ navigation }) => ({
      header: null,
    }),
  },
);

const SettingsStack = createStackNavigator(
  {
    Settings: { screen: Settings },
  },
  {
    headerMode: 'float',
    defaultNavigationOptions: ({ navigation }) => ({
      header: null,
    }),
  },
);

const TasksListStack = createStackNavigator(
  {
    TasksList: { screen: TasksList },
  },
  {
    headerMode: 'float',
    defaultNavigationOptions: ({ navigation }) => ({
      header: null,
    }),
  },
);

export default createBottomTabNavigator(
  {
    Children: {
      screen: ChildrenStack,
    },
    TasksList: {
      screen: TasksListStack,
    },
    Settings: {
      screen: SettingsStack,
    },
  },
  {
    defaultNavigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused, horizontal, tintColor }) => {
        const { routeName } = navigation.state;

        if (routeName === 'Children') {
          return <FontAwesomeIcon icon={faChild} size={24} color={tintColor} />;
        } else if (routeName === 'Settings') {
          return <FontAwesomeIcon icon={faCog} size={24} color={tintColor} />;
        } else if (routeName === 'TasksList') {
          return <FontAwesomeIcon icon={faList} size={24} color={tintColor} />;
        }
      },
    }),
    tabBarOptions: {
      activeTintColor: '#2982ed',
      inactiveTintColor: '#a1a1a1',
      tabBarButtonComponent: {
        borderColor: '#2982ed',
      },
    },
  },
);
