import { createAppContainer } from 'react-navigation';
import MainNavigation from '../navigation/MainNavigation';

export default createAppContainer(MainNavigation);
