import React, { Component } from 'react';
import AppNavigation from './navigation/AppNavigation';
import { DefaultTheme, Provider as PaperProvider } from 'react-native-paper';

console.disableYellowBox = true;

const theme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    primary: '#2982ed',
  },
};

export default class App extends Component {
  render() {
    return (
      <PaperProvider theme={theme}>
        <AppNavigation />
      </PaperProvider>
    );
  }
}
